﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace daw_webapi.Contracts.Responses
{
    public class SelectItemResponse
    {
        public string Label { get; set; }
        public long Value { get; set; }
    }
}

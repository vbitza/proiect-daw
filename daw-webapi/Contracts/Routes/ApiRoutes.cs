﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace daw_webapi.Contracts
{
    public static partial class ApiRoutes
    {
        public const string Root = "api";

        public const string Base = Root + "/";
    }
}
